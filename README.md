# Modul 164 Datenbanken erstellen und Daten einfügen

Implementiert ein logisches, relationales Datenmodell in einem Datenbankmanagementsystem.
Fügt Daten in die Datenbank ein, prüft die eingefügten
Daten und korrigiert allfällige Fehler.

Voraussetzung und zu verwendende Unterlagen sind im Modul 162 zu finden (Skript, Normalisierung, einfache Abfragen ...). Das Skript zum Modul 164 (siehe oben) ist zur Zeit noch sehr umfangreich. Nicht alle Themen sind direkt in diesem Modul relevant, aber bei Interesse können Sie die Inhalte natürlich durchschauen.